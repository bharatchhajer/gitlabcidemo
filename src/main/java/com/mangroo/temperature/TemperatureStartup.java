package com.mangroo.temperature;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ResourceInUseException;
import com.mangroo.temperature.data.Temperature;
import com.mangroo.temperature.data.repo.TemperatureRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class TemperatureStartup implements ApplicationListener<ApplicationReadyEvent> {

    Logger logger = LoggerFactory.getLogger(TemperatureStartup.class);

    @Autowired
    private TemperatureRepository temperatureRepository;

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    @Autowired
    private AmazonDynamoDB amazonDynamoDB;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        logger.info("Application Startup!");
        try {
            CreateTableRequest tableRequest = dynamoDBMapper.generateCreateTableRequest(Temperature.class);

            tableRequest.setProvisionedThroughput(new ProvisionedThroughput(1L, 1L));

            logger.info("About to create tables");
            amazonDynamoDB.createTable(tableRequest);
            logger.info("Tables created");
        } catch (
                ResourceInUseException e) {
            // Do nothing, table already created
            logger.info("Temperature table already exists");
        }
    }
}
